#include<iostream>
#include<vector>
#include <string>
#include <algorithm> 

using namespace std;
/*
char Matrix[3][3] = {'0', '1', '2', '3', '4', '5', '6', '7', '8'};
void displayMatrix()
{
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            cout << "\t" << Matrix[i][j] << " | ";
        }
        cout << endl;
    }
    
}
*/
char player_x = 'X';
char player_o = 'O';
char EMPTY = ' ';
char TIE = 'T';
char NO_ONE = 'N';

void displayMatrix(const vector<char>& b)
{
    //system("clear");
    cout <<"\n\n";
    cout << "\n\t\t" << b[0] << " || " << b[1] << " || " << b[2];
    cout << "\n\t\t" << "------------";
    cout << "\n\t\t" << b[3] << " || " << b[4] << " || " << b[5];
    cout << "\n\t\t" << "------------";
    cout << "\n\t\t" << b[6] << " || " << b[7] << " || " << b[8];
    cout <<"\n\n";
}
void displayInstructions()
{
    cout << "\n\n" <<"Willcome \n";
    cout << "to  Tic tac toe 2 players \n\n";

    cout << "Make your move by entering a number between 0-8 the Number \n";
    cout << "correspondes to the desired borad positon , as example \n";

    cout << "\n\t\t" << "0 || 1 || 2 ";
    cout << "\n\t\t" << "-----------\n";
    cout << "\n\t\t" << "3 || 4 || 5";
    cout << "\n\t\t" << "-----------\n";
    cout << "\n\t\t" << "7 || 8 || 9\n\n";
}
char askUser(string q)
{
    char r;
    do
    {
        cout << q << "(y/n): ";
        cin >> r;
    } while (r != 'y' && r != 'n');

    return r;    

}
char whoGoesFirst()
{
    char goFirst = askUser("Player X do you want to start? ");
    if (goFirst == 'y')
    {
        cout << "\n Player X please chosse your move. \n\n ";
        return player_x;
    }
    else
    {
        cout << "\n Player O please chosse your move. \n\n";
        return player_o;
    }
}
int chooseNumber(const vector<char> & board, string q, int highst, int lowest)
{
    int pos;
    
    do
    {
        cout << q << "(" << lowest << "-" << highst << "): ";
        cin >> pos;
         if(cin.fail()) 
         {
                cout << "Please Enter a number!!" << endl;
                cin.clear();
                cin.ignore(256,'\n');
                displayMatrix(board);
                cin >> pos;
         }
    } while (pos < highst || pos > lowest);
    
    //cout << q << "(" << lowest << "-" << highst << "): ";
    //cin >> pos;
    return pos;
}
int  play(const vector<char> & board, char p)
{
   
    string s =  "Please choose whre to move Player ";
    s.append(1, p);
   
    int pos = chooseNumber(board, s , 0, board.size() -1);
   
    while (board[pos] != EMPTY)
    {
        cout << "this position is not empty, please choose an Empty one: \n";
        pos = chooseNumber(board, "Please choose where to move: ",0,  board.size() -1);
    }
    
    
    cout << "OK \n";
    return pos;
}
char winner(const vector<char> & board)
{
    //all possible winng rows 
    const int WIN_ROWS[8][3] = {{0,1,2}, 
                                {3,4,5},
                                {6,7,8},
                                {0,3,6},
                                {1,4,7},
                                {2,5,8},
                                {0,4,8},
                                {2,4,6}};
    
    
    for (int i = 0; i < 8; i++)
    {
        if(board[WIN_ROWS[i][0]] != EMPTY &&
           board[WIN_ROWS[i][0]] == board[WIN_ROWS[i][1]] &&
           board[WIN_ROWS[i][1]] == board[WIN_ROWS[i][2]]
           )
        {
            return board[WIN_ROWS[i][0]];
        }
    }
    

   if (count(board.begin(), board.end(),EMPTY) == 0)
   {
       return TIE;
   }
   
    return NO_ONE;

}
void theWinnerIs(char winner, int count_x, int count_y)
{
    if (winner == player_x)
    {
        cout << "Player X has Wonn!!\n";
        cout << "With only " << count_x << " steps. \n";
    }
    else if (winner == player_o)
    {
        cout << "Player O has Wonn!!\n";
         cout << "With only " << count_y << " steps. \n";
    }
    else
    {
        cout << "It is a Tie!!\n";
    }
}

int main()
{
    vector<char> board(9,EMPTY);

    displayInstructions();
    char player = whoGoesFirst();
    char turn = player;
    int move;
    int count_x = 0;
    int count_y = 0;


    //cout << "\n" << "Player " << player << "you may start." << endl;

    
    
    while (winner(board) == NO_ONE)
    {
        
        if (turn == 'X')
        {
            move = play(board, player);
            board[move] = turn;
            turn = player_o;
            count_x ++;
        }
        else
        {
            move = play(board, player);
            board[move] = turn;
            turn = player_x;
            count_y ++;
        }
        
         displayMatrix(board);
         
    }
    theWinnerIs(winner(board), count_x, count_y);
    
    
   // displayMatrix(board);
    return 0;
}